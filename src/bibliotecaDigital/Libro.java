package bibliotecaDigital;


public class Libro {//este seria el TAD LIBRO

	private String codigoISBN;//guarda el codigo ISBN
	private String titulo;//guarda el titulo del libro
	private String categoria;//guarda de que categoria es
	private double ancho;//guarda el tama�o del ancho del lomo del libro
	
	public Libro(String ISBN,String categoria,String titulo,double elAncho){
		//es el contructor del libro
		this.codigoISBN=ISBN;
		this.titulo=titulo;
		this.categoria=categoria;
		this.ancho=elAncho;
	}
	
	//los getter nos van a permitir usar los datos del libro que fueron guardados en modo privado
	
	public String obtenerISBN() {
		return codigoISBN;
	}
	
	public String getTitulo() {
		return titulo;
	}

	public String getCategoria() {
		return categoria;	
	}

	public double getAncho() {
		return ancho;	
	}

	@Override
	public String toString() {
		return "[" + codigoISBN + ", " + titulo + ", " + ancho + "]";
	}

	
}