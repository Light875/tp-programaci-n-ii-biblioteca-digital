package bibliotecaDigital;

import java.util.LinkedList;
import java.util.HashMap;
import java.util.Iterator;


public class BDUNGS {
	private String nombre;
	private LinkedList <Estante> estantes;
	
	public BDUNGS(int cantEstantes, double tamanio) {
		// TODO Auto-generated constructor stub
		this.nombre="";//No es informacion relevante para los ejercicios, pero se supone por logica que debe existir
		this.estantes= new LinkedList <Estante>();
		for(int i=0;i<cantEstantes;i++){
			Estante nuevo= new Estante(null,tamanio);
			this.estantes.add(nuevo);
			
		}
	}

	public void rotularEstante(String categoria, int i) {
		if (estantes.get(i).getEstaRotulado()==false) {
			estantes.get(i).setRotulo(categoria);//le modifica el rotulo
			estantes.get(i).setEstaRotulado(true);
		}
		else {
			throw new RuntimeException("No es posible rotular este estante");
		}
	}

	public boolean ingresarLibro(String isbn, String cat, String titulo, int ancho) {
		if(existeCategoria(cat)) {
			for (int i=0; i<estantes.size();i++) {
				if (estantes.get(i).getRotulo()==cat) {
					if(estantes.get(i).getEspacioLibre()>=ancho) {
						estantes.get(i).guardarLibro(isbn, cat, titulo, ancho);
						return true;
					}	
				}
			}
		}
		else {
			throw new RuntimeException("No existe ningun estante con dicha categoria");
		}
		return false;
		
	}


	public double espacioLibre(int i) {
		if (estantes.get(i).getEstaRotulado())
			return estantes.get(i).getEspacioLibre();
		else
			throw new RuntimeException("No esta rotulado");
	}
	

	public HashMap<String, Integer> verLibrosCategoria(String categoria) {
		HashMap<String,Integer> librosDeCategoria = new HashMap<String,Integer>();
		if(!existeCategoria(categoria)) {
			throw new RuntimeException("No se encuentra dicha categoria");
		}
		for (Estante est: this.estantes) {
			if (est.getRotulo()==categoria) {
				for (Libro libro : est.getEstante()) {
					if (!librosDeCategoria.containsKey(libro.obtenerISBN())) {
						librosDeCategoria.put(libro.obtenerISBN(), 1);
					}
					else {
						librosDeCategoria.replace(libro.obtenerISBN(), librosDeCategoria.get(libro.obtenerISBN() + 1));
					}
				}
			}
		}
		return librosDeCategoria;
	}

	public void eliminarLibro(String isbn) {
		Iterator<Estante> it=estantes.iterator();
		while (it.hasNext()) {
			Estante est = (Estante) it.next();
			est.eliminarLibro(isbn);
		}
		
	}
	

	public int reacomodarCategoria(String categoria) {
		int cont=0;
		if(!existeCategoria(categoria)) {
			throw new RuntimeException("No se encuentra dicha categoria");
		}
		else {
			for (Estante est: this.estantes) {
				if (est.getRotulo()==categoria) {
					reacomodarLibros(est,estanteConMasEspacio(categoria));
				}
			}
			if (estanteConMasEspacio(categoria).estaVacio()) {
				estanteConMasEspacio(categoria).setRotulo(null);
				estanteConMasEspacio(categoria).setEstaRotulado(false);
				cont++;
			}
		}
		return cont;
	}

	private boolean existeCategoria(String categoria) {
		boolean ret=false;
		for (Estante est: this.estantes) {
			ret = ret || est.getRotulo()==categoria;
		}
		return ret;
	}

	private Estante estanteConMasEspacio(String cat) {
		Estante vacio = estantes.get(0);
		for (Estante est: this.estantes) {
			if (est.getRotulo()==cat && est.getEspacioLibre()>vacio.getEspacioLibre()) {
				vacio=est;
			}
		}
		return vacio;
	}
	
	private void reacomodarLibros(Estante e1, Estante e2) {
		Iterator<Libro> it=e2.getEstante().iterator();
		while (it.hasNext()) {
			Libro l = (Libro) it.next();
			if (l.getAncho()<=e1.getEspacioLibre()) {
				e1.guardarLibro(l.obtenerISBN(), l.getCategoria(), l.getTitulo(), l.getAncho());;
				e2.eliminarLibro(l.obtenerISBN());
			}
		}
	}

	@Override
	public String toString() {
		StringBuilder s=new StringBuilder();
		s.append("Biblioteca Digital, posee los siguientes estantes: \n");
		for(Estante est: this.estantes) {
			if (est.getEstaRotulado()) {
				s.append(est + "\n");
			}
		}
		return s.toString();
	}
	
	
}
