package bibliotecaDigital;

import java.util.Iterator;
import java.util.LinkedList;

public class Estante {
	
	private String rotulo;//es el nombre de la categoria que contiene
	private boolean estaRotulado;
	private double tamanio;
	private double espacioLibre;//sera el que tambien contenga el valor del estante pero este sera el que modificaremos czada vez que se ingrese o se quite un libro
	private LinkedList <Libro> estante;
	
	public Estante(String categoria, double anchoDelEstante){
		this.rotulo = categoria;
		this.estaRotulado = false;
		this.tamanio = anchoDelEstante;
		this.espacioLibre = anchoDelEstante;
		this.estante = new LinkedList<Libro>();
	}
	
	public void eliminarLibro(String isbn) {
		Iterator<Libro> it=estante.iterator();
		while (it.hasNext()) {
			Libro l = (Libro) it.next();
			if (l.obtenerISBN()==isbn) {
				this.espacioLibre=this.espacioLibre+l.getAncho();
				estante.remove(l);
			}
		}
	}
	
	public boolean estaVacio() {
		if (this.espacioLibre==this.tamanio && estante.size()==0) {
			return true;
		}
		return false;
	}
	
	public String obtenerISBNDeLibro(int f) {
		return estante.get(f).obtenerISBN();
	}
	
	public void setRotulo(String categoria){
		//Solo se le puede poner rotulo si est� vacio, lo cu�l se determina con el boolean estaRotulado
		this.rotulo=categoria;
	}

	public String getRotulo(){
		return rotulo;
	}
	
	public double getTamanio(){
		return tamanio;
	}

	public double getEspacioLibre(){
		return espacioLibre;
	}	

	public boolean getEstaRotulado() {
		return estaRotulado;
	}

	public void setEstaRotulado(boolean estaRotulado) {
		this.estaRotulado = estaRotulado;
	}
	
	
	public LinkedList <Libro> getEstante(){
		return estante;
	}

	
	public void guardarLibro(String elISBN, String categoria, String titulo,double ancho){
		/** lo que hace es sumarle espacio libre al estante luego de quitar un libro (en nuestro caso una colecci�n) */
		Libro elLibro=new Libro(elISBN, categoria, titulo, ancho);
		estante.add(elLibro);
		this.espacioLibre=this.espacioLibre-ancho;
	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append(this.rotulo + ", " + this.tamanio + " cm, posee los siguientes libros: \n");
		for (Libro libro: this.estante) {
			s.append(libro + "\n");
		}
		return s.toString();
	}


}