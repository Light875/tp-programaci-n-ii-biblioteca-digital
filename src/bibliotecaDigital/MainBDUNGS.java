package bibliotecaDigital;

public class MainBDUNGS {

	public static void main(String[] args){
			
			/**
			 * Constructor
			 * 
			 * param1: cantidad de estantes,
			 * param2: ancho de la biblioteca en cm)
			 * 
			 */
			BDUNGS bd = new BDUNGS(10, 100);
			
			/**
			 * Rotulado de estantes
			 * 
			 * param1: Categoria,
			 * param2: numero de orden del estante
			 */
			bd.rotularEstante("Computacion", 1);
			bd.rotularEstante("Matematica", 2);
			System.out.println(bd);
			
			/**
			 * registrar un nuevo libro.
			 * Si ya existe, incrementa la cantidad de ejemplares.
			 * 
			 * param1: Codigo ISBN,//no diferencia obras sino las ediciones
			 * param2: categoria,
			 * param3: titulo,
			 * param4: ancho en cm
			 */
			bd.ingresarLibro("9789684443457", "Computacion", "Estructuras de datos", 5);
			bd.ingresarLibro("9788415552222", "Computacion", "Estructuras de datos en Java", 7);
			bd.ingresarLibro("9389557783457", "Matematica", "Analisis de Funciones", 4);
			System.out.println(bd);
			// Imprime la info y bonito el estado actual de la biblioteca (de todos sus estantes)
			
			/**
			 * Elimina todos los ejemplares de un libro por su codigo ISBN
			 */
			
			bd.eliminarLibro("9389557783457");
			System.out.println(bd);
			
			/**
			 * Cambia la categoria asociada a un estante determinado por su numero de orden.
			 * 
			 * param1: nueva categoria
			 * param2: numero de orden del estante
			 */
			//bd.rotularEstante("An�lisis Matem�tico", 2);
			
			// Imprime linfo y bonito el estado actual de la biblioteca (de todos sus estantes)
			}
}
